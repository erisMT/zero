local S = minetest.get_translator("zr_acacia");

-- GATE
zr_door.register_gate("zr_acacia:gate", {
	description = S("Acacia Wood Fence Gate"),
	texture = "zr_acacia_wood.png",
	material = "zr_acacia:wood",
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
	sounds = zr_wood.sounds,
})
minetest.register_alias("acacia:gate", "zr_acacia:gate")
