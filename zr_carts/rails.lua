-- carts/rails.lua

-- support for MT game translation.
local S = zr_carts.get_translator

zr_carts:register_rail("zr_carts:rail", {
	description = S("Rail"),
	tiles = {
		"zr_carts_rail_straight.png", "zr_carts_rail_curved.png",
		"zr_carts_rail_t_junction.png", "zr_carts_rail_crossing.png"
	},
	inventory_image = "zr_carts_rail_straight.png",
	wield_image = "zr_carts_rail_straight.png",
	groups = zr_carts:get_rail_groups(),
}, {})
minetest.register_alias("carts:rail", "zr_carts:rail")

minetest.register_craft({
	output = "zr_carts:rail 18",
	recipe = {
		{"zr_iron:ingot", "group:wood", "zr_iron:ingot"},
		{"zr_iron:ingot", "", "zr_iron:ingot"},
		{"zr_iron:ingot", "group:wood", "zr_iron:ingot"},
	}
})

zr_carts:register_rail("zr_carts:powerrail", {
	description = S("Powered Rail"),
	tiles = {
		"zr_carts_rail_straight_pwr.png", "zr_carts_rail_curved_pwr.png",
		"zr_carts_rail_t_junction_pwr.png", "zr_carts_rail_crossing_pwr.png"
	},
	groups = zr_carts:get_rail_groups(),
}, {acceleration = 5})
minetest.register_alias("carts:powerrail", "zr_carts:powerrail")

minetest.register_craft({
	output = "zr_carts:powerrail 18",
	recipe = {
		{"zr_iron:ingot", "group:wood", "zr_iron:ingot"},
		{"zr_iron:ingot", "zr_mese:crystal", "zr_iron:ingot"},
		{"zr_iron:ingot", "group:wood", "zr_iron:ingot"},
	}
})


zr_carts:register_rail("zr_carts:brakerail", {
	description = S("Brake Rail"),
	tiles = {
		"zr_carts_rail_straight_brk.png", "zr_carts_rail_curved_brk.png",
		"zr_carts_rail_t_junction_brk.png", "zr_carts_rail_crossing_brk.png"
	},
	groups = zr_carts:get_rail_groups(),
}, {acceleration = -3})
minetest.register_alias("carts:brakerail", "zr_carts:brakerail")

minetest.register_craft({
	output = "zr_carts:brakerail 18",
	recipe = {
		{"zr_iron:ingot", "group:wood", "zr_iron:ingot"},
		{"zr_iron:ingot", "zr_coal:lump", "zr_iron:ingot"},
		{"zr_iron:ingot", "group:wood", "zr_iron:ingot"},
	}
})
