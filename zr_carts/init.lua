-- carts/init.lua

-- Load support for MT game translation.
local S = minetest.get_translator("zr_carts")

zr_carts = {}
zr_carts.modpath = minetest.get_modpath("zr_carts")
zr_carts.railparams = {}
zr_carts.get_translator = S

-- Maximal speed of the cart in m/s (min = -1)
zr_carts.speed_max = 7
-- Set to -1 to disable punching the cart from inside (min = -1)
zr_carts.punch_speed_max = 5
-- Maximal distance for the path correction (for dtime peaks)
zr_carts.path_distance_max = 3


dofile(zr_carts.modpath.."/functions.lua")
dofile(zr_carts.modpath.."/rails.lua")
dofile(zr_carts.modpath.."/cart_entity.lua")

-- Register rails as dungeon loot
if minetest.global_exists("zr_dungeon_loot") then
	zr_dungeon_loot.register({
		name = "zr_carts:rail", chance = 0.35, count = {1, 6}
	})
end
