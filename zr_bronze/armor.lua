
local S = minetest.get_translator("zr_bronze");

-- Boots
simple_armor.register("zr_bronze:boots", {
	description = S("Bronze Boots"),
	inventory_image = "zr_bronze_boots_item.png",
	armor = {
		slot = "feet",
		groups = { fleshy = 7 }, 
		texture = "zr_bronze_boots.png",
	},
})
simple_armor.boots_recipe("zr_bronze:boots", "zr_bronze:bronze")
minetest.register_alias("bronze:boots", "zr_bronze:boots")

-- Chestplate
simple_armor.register("zr_bronze:chestplate", {
	description = S("Bronze Boots"),
	inventory_image = "zr_bronze_chestplate_item.png",
	armor = {
		slot = "torso",
		groups = { fleshy = 15 }, 
		texture = "zr_bronze_chestplate.png",
	},
})
simple_armor.chestplate_recipe("zr_bronze:chestplate", "zr_bronze:bronze")
minetest.register_alias("bronze:chestplate", "zr_bronze:chestplate")

-- Leggings
simple_armor.register("zr_bronze:leggings", {
	description = S("Bronze Boots"),
	inventory_image = "zr_bronze_leggings_item.png",
	armor = {
		slot = "legs",
		groups = { fleshy = 13 }, 
		texture = "zr_bronze_leggings.png",
	},
})
simple_armor.leggings_recipe("zr_bronze:leggings", "zr_bronze:bronze")
minetest.register_alias("bronze:leggings", "zr_bronze:leggings")

-- Helmet
simple_armor.register("zr_bronze:helmet", {
	description = S("Bronze Boots"),
	inventory_image = "zr_bronze_helmet_item.png",
	armor = {
		slot = "head",
		groups = { fleshy = 9 }, 
		texture = "zr_bronze_helmet.png",
	},
})
simple_armor.helmet_recipe("zr_bronze:helmet", "zr_bronze:bronze")
minetest.register_alias("bronze:helmet", "zr_bronze:helmet")

