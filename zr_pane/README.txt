Minetest Game mod: pane
=========================
See license.txt for license information.

Authors of source code
----------------------
Originally by xyz (MIT)
BlockMen (MIT)
sofar (MIT)
Various Minetest developers and contributors (MIT)

Authors of media (textures)
---------------------------
xyz (CC BY-SA 3.0):
  All textures not mentioned below.

Gambit (CC BY-SA 3.0):
  pane_bar.png

paramat (CC BY-SA 3.0):
  pane_bar_top.png

Krock (CC0 1.0):
  pane_edge.png

TumeniNodes (CC BY-SA 3.0):
  pane_door_steel_bar.png
  pane_item_steel_bar.png
  pane_trapdoor_steel_bar.png
  pane_trapdoor_steel_bar_side.png
  pane_steel_bar_door_close.ogg
  pane_steel_bar_door_open.ogg
