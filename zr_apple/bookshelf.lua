
local S = minetest.get_translator("zr_apple");

zr_paper.register_bookshelf("zr_apple:bookshelf", {
	description = "Apple Wood Bookshelf",
	texture = "zr_apple_wood.png",
})
minetest.register_alias("apple:bookshelf", "zr_apple:bookshelf")
