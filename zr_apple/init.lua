
zr_apple = {}

local mod_path = minetest.get_modpath("zr_apple")

dofile(mod_path.."/apple.lua")
dofile(mod_path.."/sapling.lua")
dofile(mod_path.."/bush.lua")
dofile(mod_path.."/bush_sapling.lua")
dofile(mod_path.."/mapgen.lua")

if (minetest.get_modpath("zr_fence") ~= nil) then
	dofile(mod_path.."/fence.lua")
end

if (minetest.get_modpath("zr_door") ~= nil) then
	dofile(mod_path.."/door.lua")
end

if (minetest.get_modpath("zr_stair") ~= nil) then
	dofile(mod_path.."/stair.lua")
end

if (minetest.get_modpath("zr_paper") ~= nil) then
	dofile(mod_path.."/bookshelf.lua")
end

if (minetest.get_modpath("zr_mese") ~= nil) then
	dofile(mod_path.."/meselamp.lua")
end
