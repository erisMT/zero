
local S = minetest.get_translator("zr_apple");

minetest.register_node("zr_apple:bush_stem", {
	description = S("Bush Stem"),
	drawtype = "plantlike",
	visual_scale = 1.41,
	tiles = {"zr_apple_bush_stem.png"},
	inventory_image = "zr_apple_bush_stem.png",
	wield_image = "zr_apple_bush_stem.png",
	paramtype = "light",
	sunlight_propagates = true,
	groups = {choppy = 2, oddly_breakable_by_hand = 1, flammable = 2},
	sounds = zr_wood.sounds,
	selection_box = {
		type = "fixed",
		fixed = {-7 / 16, -0.5, -7 / 16, 7 / 16, 0.5, 7 / 16},
	},
})
minetest.register_alias("apple:bush_stem", "zr_apple:bush_stem")

minetest.register_node("zr_apple:bush_leaves", {
	description = S("Bush Leaves"),
	drawtype = "allfaces_optional",
	tiles = {"zr_apple_leaves_simple.png"},
	paramtype = "light",
    groups = {snappy = 3, leafdecay = 3, flammable = 2, leaves = 1, oddly_breakable_by_hand=3},
	drop = {
		max_items = 1,
		items = {
			{items = {"zr_apple:bush_sapling"}, rarity = 5},
			{items = {"zr_apple:bush_leaves"}}
		}
	},
	sounds = zr_wood.leaves_sounds,

	after_place_node = zr_wood.after_place_leaves,
})
minetest.register_alias("apple:bush_leaves", "zr_apple:bush_leaves")
