
local S = minetest.get_translator("zr_dirt")

zr_gravel.sounds = {
    footstep = {name = "zr_gravel_footstep", gain = 0.1},
    dig = {name = "zr_gravel_dig", gain = 0.35},
    dug = {name = "zr_gravel_dug", gain = 1.0},
    place = {name = "zr_gravel_footstep", gain = 1.0},
}

local drop_items = {
	{items = {"zr_gravel:gravel"}}
}

if (minetest.get_modpath("zr_fire") ~= nil) then
	table.insert(drop_items, 1, {items = {"zr_fire:flint"}, rarity = 16})
end

minetest.register_node("zr_gravel:gravel", {
    description = S("Gravel"),
    tiles = {"zr_gravel.png"},
    groups = {crumbly = 2, falling_node = 1},
    sounds = zr_gravel.sounds,
    drop = {
        max_items = 1,
        items = drop_items,
    }
})
minetest.register_alias("gravel:gravel", "zr_gravel:gravel")
minetest.register_alias("gravel", "zr_gravel:gravel")
