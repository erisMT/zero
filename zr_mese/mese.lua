
local S = minetest.get_translator("zr_mese");

local mese_metal = zr_metal.register_metal("zr_mese", {
	block = {description = S("Mese Block"), png = "zr_mese_block.png"},
	ore =   {description = S("Mese Ore"), png = "zr_mese_mineral.png", drop = "zr_mese:crystal"},
})
minetest.register_alias("mese:block", "zr_mese:block")
minetest.register_alias("mese:ore", "zr_mese:ore")

minetest.register_craftitem("zr_mese:crystal", {
	description = S("Mese Crystal"),
	inventory_image = "zr_mese_crystal.png",
})
minetest.register_alias("mese:crystal", "zr_mese:crystal")

minetest.register_craftitem("zr_mese:crystal_fragment", {
	description = S("Mese Crystal Fragment"),
	inventory_image = "zr_mese_crystal_fragment.png",
})
minetest.register_alias("mese:crystal_fragment", "zr_mese:crystal_fragment")

-- crafting
minetest.register_craft({
	output = mese_metal.block,
	recipe = {
		{"zr_mese:crystal", "zr_mese:crystal", "zr_mese:crystal"},
		{"zr_mese:crystal", "zr_mese:crystal", "zr_mese:crystal"},
		{"zr_mese:crystal", "zr_mese:crystal", "zr_mese:crystal"},
	}
})

minetest.register_craft({
	output = "zr_mese:crystal",
	recipe = {
		{"zr_mese:crystal_fragment", "zr_mese:crystal_fragment", "zr_mese:crystal_fragment"},
		{"zr_mese:crystal_fragment", "zr_mese:crystal_fragment", "zr_mese:crystal_fragment"},
		{"zr_mese:crystal_fragment", "zr_mese:crystal_fragment", "zr_mese:crystal_fragment"},
	}
})

minetest.register_craft({
	output = "zr_mese:crystal 9",
	recipe = {
		{mese_metal.block},
	}
})

minetest.register_craft({
	output = "zr_mese:crystal_fragment 9",
	recipe = {
		{"zr_mese:crystal"},
	}
})

-- register ores
if (mese_metal.ore ~= nil) then
    minetest.register_ore({
        ore_type       = "scatter",
        ore            = mese_metal.ore,
        wherein        = "mapgen_stone",
        clust_scarcity = 14 * 14 * 14,
        clust_num_ores = 5,
        clust_size     = 3,
        y_max          = 31000,
        y_min          = 1025,
    })

    minetest.register_ore({
        ore_type       = "scatter",
        ore            = mese_metal.ore,
        wherein        = "mapgen_stone",
        clust_scarcity = 18 * 18 * 18,
        clust_num_ores = 3,
        clust_size     = 2,
        y_max          = -64,
        y_min          = -255,
    })

    minetest.register_ore({
        ore_type       = "scatter",
        ore            = mese_metal.ore,
        wherein        = "mapgen_stone",
        clust_scarcity = 14 * 14 * 14,
        clust_num_ores = 5,
        clust_size     = 3,
        y_max          = -256,
        y_min          = -31000,                                                                                                                                                                                                             
    })
end

-- tools
if(minetest.get_modpath("zr_tools")) ~= nil then
	minetest.register_tool("zr_mese:pick", {
		description = S("Mese Pickaxe"),
		inventory_image = "zr_mese_pick.png",
		tool_capabilities = {
			full_punch_interval = 0.9,
			max_drop_level=3,
			groupcaps={
				cracky = {times={[1]=2.4, [2]=1.2, [3]=0.60}, uses=20, maxlevel=3},
			},
			damage_groups = {fleshy=5},
		},
		sound = zr_tools.sound,
		groups = {pickaxe = 1}
	})
	minetest.register_alias("mese:pick", "zr_mese:pick")

	minetest.register_tool("zr_mese:shovel", {
		description = S("Mese Shovel"),
		inventory_image = "zr_mese_shovel.png",
		wield_image = "zr_mese_shovel.png^[transformR90",
		tool_capabilities = {
			full_punch_interval = 1.0,
			max_drop_level=3,
			groupcaps={
				crumbly = {times={[1]=1.20, [2]=0.60, [3]=0.30}, uses=20, maxlevel=3},
			},
			damage_groups = {fleshy=4},
		},
		sound = zr_tools.sound,
		groups = {shovel = 1}
	})
	minetest.register_alias("mese:shovel", "zr_mese:shovel")

	minetest.register_tool("zr_mese:axe", {
		description = S("Mese Axe"),
		inventory_image = "zr_mese_axe.png",
		tool_capabilities = {
			full_punch_interval = 0.9,
			max_drop_level=1,
			groupcaps={
				choppy={times={[1]=2.20, [2]=1.00, [3]=0.60}, uses=20, maxlevel=3},
			},
			damage_groups = {fleshy=6},
		},
		sound = zr_tools.sound,
		groups = {axe = 1}
	})
	minetest.register_alias("mese:axe", "zr_mese:axe")

	minetest.register_tool("zr_mese:sword", {
		description = S("Mese Sword"),
		inventory_image = "zr_mese_sword.png",
		tool_capabilities = {
			full_punch_interval = 0.7,
			max_drop_level=1,
			groupcaps={
				snappy={times={[1]=2.0, [2]=1.00, [3]=0.35}, uses=30, maxlevel=3},
			},
			damage_groups = {fleshy=7},
		},
		sound = zr_tools.sound,
		groups = {sword = 1}
	})
	minetest.register_alias("mese:sword", "zr_mese:sword")
end

if(minetest.get_modpath("zr_mesecons_wires")) ~= nil then
	minetest.register_craft({
		type = "cooking",
		output = "zr_mesecons:wire_00000000_off 2",
		recipe = "zr_mese:crystal_fragment",
		cooktime = 3,
	})

	minetest.register_craft({
		type = "cooking",
		output = "zr_mesecons:wire_00000000_off 18",
		recipe = "zr_mese:crystal",
		cooktime = 15,
	})
end

