
zr_mese = {}

local mod_path = minetest.get_modpath("zr_mese")
dofile(mod_path.."/mese.lua")

if (minetest.get_modpath("zr_wood") ~= nil and minetest.get_modpath("zr_glass") ~= nil) then
	dofile(mod_path.."/lamp.lua")
end
