zr_water = {}

local mod_path = minetest.get_modpath("zr_water")
dofile(mod_path.."/water.lua")
dofile(mod_path.."/alias.lua")

if minetest.global_exists("zr_bucket") then
	dofile(mod_path.."/water_bucket.lua")
end
