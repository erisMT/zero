
zr_sand = {};

local mod_path = minetest.get_modpath("zr_sand")

dofile(mod_path.."/sand.lua")
dofile(mod_path.."/desert.lua")
dofile(mod_path.."/silver.lua")

if minetest.get_modpath("zr_stair") ~= nil then
	dofile(mod_path.."/stair.lua")
end
