local S = minetest.get_translator("zr_sand")

-- SILVER SAND
minetest.register_node("zr_sand:silver", {
	description = S("Silver Sand"),
	tiles = {"zr_sand_silver.png"},
	groups = {crumbly = 3, falling_node = 1, sand = 1},
    sounds = zr_sand.sounds,
})

minetest.register_node("zr_sand:silver_sandstone", {
	description = S("Silver Sandstone"),
	tiles = {"zr_sand_silver_sandstone.png"},
	groups = {crumbly = 1, cracky = 3},
	sounds = zr_stone.sounds,
})

minetest.register_node("zr_sand:silver_sandstone_brick", {
	description = S("Silver Sandstone Brick"),
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {"zr_sand_silver_sandstone_brick.png"},
	is_ground_content = false,
	groups = {cracky = 2},
	sounds = zr_stone.sounds,
})

minetest.register_node("zr_sand:silver_sandstone_block", {
	description = S("Silver Sandstone Block"),
	tiles = {"zr_sand_silver_sandstone_block.png"},
	is_ground_content = false,
	groups = {cracky = 2},
	sounds = zr_stone.sounds,
})
minetest.register_alias("sand:silver", "zr_sand:silver")
minetest.register_alias("sand:silver_sandstone", "zr_sand:silver_sandstone")
minetest.register_alias("sand:silver_sandstone_brick", "zr_sand:silver_sandstone_brick")
minetest.register_alias("sand:silver_sandstone_block", "zr_sand:silver_sandstone_block")
