
zr_stair.register_all("zr_sand:desert_stone_",{ recipeitem = "zr_sand:desert_stone" }) 
zr_stair.register_all("zr_sand:desert_cobble_",{ recipeitem = "zr_sand:desert_cobble" }) 
zr_stair.register_all("zr_sand:desert_stone_brick_",{ recipeitem = "zr_sand:desert_stone_brick" }) 
zr_stair.register_all("zr_sand:desert_stone_block_",{ recipeitem = "zr_sand:desert_stone_block" }) 

zr_stair.register_all("zr_sand:sandstone_",{ recipeitem = "zr_sand:sandstone" }) 
zr_stair.register_all("zr_sand:sandstone_brick_",{ recipeitem = "zr_sand:sandstone_brick" }) 
zr_stair.register_all("zr_sand:sandstone_block_",{ recipeitem = "zr_sand:sandstone_block" }) 

zr_stair.register_all("zr_sand:desert_sandstone_",{ recipeitem = "zr_sand:desert_sandstone" }) 
zr_stair.register_all("zr_sand:desert_sandstone_brick_",{ recipeitem = "zr_sand:desert_sandstone_brick" }) 
zr_stair.register_all("zr_sand:desert_sandstone_block_",{ recipeitem = "zr_sand:desert_sandstone_block" }) 

zr_stair.register_all("zr_sand:silver_sandstone_",{ recipeitem = "zr_sand:silver_sandstone" }) 
zr_stair.register_all("zr_sand:silver_sandstone_brick_",{ recipeitem = "zr_sand:silver_sandstone_brick" }) 
zr_stair.register_all("zr_sand:silver_sandstone_block_",{ recipeitem = "zr_sand:silver_sandstone_block" }) 
