local S = minetest.get_translator("zr_sand")

-- NORMAL SAND
zr_sand.sounds = {
    footstep = {name = "zr_sand_footstep", gain = 0.05},
    dig = zr_dirt.sounds.dig,
    dug = {name = "zr_sand_footstep", gain = 0.15},
    place = zr_dirt.sounds.place,
}

minetest.register_node("zr_sand:sand", {
    description = S("Sand"),
    tiles = {"zr_sand.png"},
    groups = {crumbly = 3, falling_node = 1, sand = 1, oddly_breakable_by_hand= 3},
    sounds = zr_sand.sounds,
})
minetest.register_alias("sand:sand", "zr_sand:sand")
minetest.register_alias("sand", "zr_sand:sand")

minetest.register_node("zr_sand:sandstone", {
	description = S("Sandstone"),
	tiles = {"zr_sand_sandstone.png"},
	groups = {crumbly = 1, cracky = 3},
	sounds = zr_stone.sounds,
})
minetest.register_alias("sand:sandstone", "zr_sand:sandstone")

minetest.register_node("zr_sand:sandstone_brick", {
	description = S("Sandstone Brick"),
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {"zr_sand_sandstone_brick.png"},
	is_ground_content = false,
	groups = {cracky = 2},
	sounds = zr_stone.sounds,
})
minetest.register_alias("sand:sandstone_brick", "zr_sand:sandstone_brick")

minetest.register_node("zr_sand:sandstone_block", {
	description = S("Sandstone Block"),
	tiles = {"zr_sand_sandstone_block.png"},
	is_ground_content = false,
	groups = {cracky = 2},
	sounds = zr_stone.sounds,
})
minetest.register_alias("sand:sandstone_block", "zr_sand:sandstone_block")
