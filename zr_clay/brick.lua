
local S = minetest.get_translator("zr_clay");

minetest.register_node("zr_clay:brick_block", {
	description = S("Brick Block"),
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {
		"zr_clay_brick_block.png^[transformFX",
		"zr_clay_brick_block.png",
	},
	is_ground_content = false,
	groups = {cracky = 3},
	sounds = zr_stone.sounds,
})
minetest.register_alias("clay:brick_block", "zr_clay:brick_block")

minetest.register_craftitem("zr_clay:brick", {
	description = S("Clay Brick"),
	inventory_image = "zr_clay_brick.png",
})
minetest.register_alias("clay:brick", "zr_clay:brick")
minetest.register_alias("brick", "zr_clay:brick")

minetest.register_craft({
	output = "zr_clay:brick_block",
	recipe = {
		{"zr_clay:brick", "zr_clay:brick"},
		{"zr_clay:brick", "zr_clay:brick"},
	}
})

minetest.register_craft({
	output = "zr_clay:brick 4",
	recipe = {
		{"zr_clay:brick_block"},
	}
})

minetest.register_craft({
	type = "cooking",
	output = "zr_clay:brick",
	recipe = "zr_clay:lump",
})
