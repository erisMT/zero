
local S = minetest.get_translator("zr_glass")

zr_glass.sounds = {
	footstep = {name = "zr_glass_footstep", gain = 0.3},
    dig = {name = "zr_glass_footstep", gain = 0.5},
    dug = {name = "zr_glass_break", gain = 1.0},
    place = {name = "zr_glass_footstep", gain = 1.0},
}

minetest.register_node("zr_glass:glass", {
    description = S("Glass"),
    drawtype = "glasslike_framed_optional",
    tiles = {"zr_glass.png", "zr_glass_detail.png"},
    paramtype = "light",
    paramtype2 = "glasslikeliquidlevel",
    sunlight_propagates = true,
    is_ground_content = false,
    groups = {cracky = 3, oddly_breakable_by_hand = 3},
    sounds = zr_glass.sounds,
})
minetest.register_alias("glass:glass", "zr_glass:glass")
minetest.register_alias("glass", "zr_glass:glass")

minetest.register_craft({
    type = "cooking",
    output = "zr_glass:glass",
    recipe = "group:sand",
})

minetest.register_craftitem("zr_glass:fragments", {
	description = S("Glass Fragments"),
	inventory_image = "zr_glass_fragments.png",
})
minetest.register_alias("glass:fragments", "zr_glass:fragments")

minetest.register_craft({
	type = "cooking",
	output = "zr_glass:glass",
	recipe = "zr_glass:fragments",
})

if minetest.global_exists("zr_dungeon_loot") then
	zr_dungeon_loot.register({
		name = "zr_glass:fragments", chance = 0.35, count = {1, 4}
	})
end
