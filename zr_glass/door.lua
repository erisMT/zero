
local S = minetest.get_translator("zr_glass");

-- DOOR
zr_door.register("zr_glass:door", {
	--tiles = {{name="zr_glass_door.png", backface_culling = true}},
	tiles = {{name="zr_glass_door.png", backface_culling = true}},
	inventory_image = "zr_glass_door_item.png",
	wield_image = "zr_glass_door_item.png",
	description = S("Glass Door"),
	groups = {node = 1, cracky=3, oddly_breakable_by_hand=3},
	sounds = zr_glass.sounds,
	sound_open = "zr_glass_door_open",
	sound_close = "zr_glass_door_close",
})
minetest.register_alias("glass:door", "zr_glass:door")

minetest.register_craft( {
	output = "zr_glass:door",
	recipe = {
		{"zr_glass:glass", "zr_glass:glass"},
		{"zr_glass:glass", "zr_glass:glass"},
		{"zr_glass:glass", "zr_glass:glass"},
	},
})

zr_door.register("zr_glass:obsidian_door", {
		tiles = {{name = "zr_glass_obsidian_door.png", backface_culling = true}},
		description = S("Obsidian Glass Door"),
		inventory_image = "zr_glass_obsidian_door_item.png",
		groups = {node = 1, cracky=3},
		sounds = zr_glass.sounds,
		sound_open = "zr_glass_door_open",
		sound_close = "zr_glass_door_close",
})
minetest.register_alias("glass:obsidian_door", "zr_glass:obsidian_door")

minetest.register_craft( {
	output = "zr_glass:obsidian_door",
	recipe = {
		{"zr_glass:obsidian", "zr_glass:obsidian"},
		{"zr_glass:obsidian", "zr_glass:obsidian"},
		{"zr_glass:obsidian", "zr_glass:obsidian"},
	},
})
