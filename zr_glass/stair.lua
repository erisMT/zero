
zr_stair.register_stair("zr_glass:", { 
	recipeitem = "zr_glass:glass",
	tiles = {
		"zr_glass_stair_split.png", "zr_glass.png",
		"zr_glass_stairside.png^[transformFX", "zr_glass_stairside.png",
		"zr_glass.png", "zr_glass_stair_split.png",
	},
	use_texture_alpha = "clip", -- only needed for stairs API
})
minetest.register_alias("glass:stair", "zr_glass:stair")

zr_stair.register_slab("zr_glass:", { 
	recipeitem = "zr_glass:glass",
	tiles = {"zr_glass.png", "zr_glass.png", "zr_glass_stair_split.png"},
	use_texture_alpha = "clip", -- only needed for stairs API
})
minetest.register_alias("glass:slab", "zr_glass:slab")

zr_stair.register_inner("zr_glass:", { 
	recipeitem = "zr_glass:glass",
	use_texture_alpha = "clip", -- only needed for stairs API
	tiles = {
		"zr_glass_stairside.png^[transformR270", "zr_glass.png",
		"zr_glass_stairside.png^[transformFX", "zr_glass.png",
		"zr_glass.png", "zr_glass_stairside.png"
	},
})
minetest.register_alias("glass:stair_inner", "zr_glass:stair_inner")

zr_stair.register_outer("zr_glass:", { 
	recipeitem = "zr_glass:glass",
	use_texture_alpha = "clip", -- only needed for stairs API
	tiles = {
		"zr_glass_stairside.png^[transformR90", "zr_glass.png",
		"zr_glass_outer_stairside.png", "zr_glass_stairside.png",
		"zr_glass_stairside.png^[transformR90","zr_glass_outer_stairside.png"
	},
})
minetest.register_alias("glass:stair_outer", "zr_glass:stair_outer")
