Minetest mod: initial_stuff
=====================================
MIT license - See license.txt for license information.

Give player initial stuff on first login, useful for servers

Usage
-----
In game.conf, just set intial_stuff 

	initial_stuff = apple:apple 1,protector:protect 

if not set, no stuff will be given
