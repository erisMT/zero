
zr_blueberry = {}

local mod_path = minetest.get_modpath("zr_blueberry")

dofile(mod_path.."/blueberry.lua")
dofile(mod_path.."/sapling.lua")
dofile(mod_path.."/mapgen.lua")
