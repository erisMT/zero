local S = minetest.get_translator("zr_blueberry");

function zr_blueberry.grow_sapling(pos) 

	if not zr_wood.check_grow_tree(pos) then
        minetest.get_node_timer(pos):start(math.random(300, 1500))
		return
	end

    local tree_schema = minetest.get_modpath("zr_blueberry") .. "/schematics/zr_blueberry_bush.mts"
    minetest.place_schematic({x=pos.x-3, y=pos.y-1, z=pos.z-3}, tree_schema, "random", nil, false)
end

minetest.register_node("zr_blueberry:sapling", {
	description = S("Blueberry Bush Sapling"),
	drawtype = "plantlike",
	tiles = {"zr_blueberry_bush_sapling.png"},
	inventory_image = "zr_blueberry_bush_sapling.png",
	wield_image = "zr_blueberry_bush_sapling.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	on_timer = zr_blueberry.grow_sapling,
	selection_box = {
		type = "fixed",
		fixed = {-4 / 16, -0.5, -4 / 16, 4 / 16, 2 / 16, 4 / 16}
	},
	groups = {snappy = 2, dig_immediate = 3, flammable = 2,
		attached_node = 1, sapling = 1},
	sounds = zr_wood.leaves_sounds,

	on_construct = function(pos)
		minetest.get_node_timer(pos):start(math.random(300, 1500))
	end,

	on_place = function(itemstack, placer, pointed_thing)
		itemstack = zr_wood.sapling_on_place(itemstack, placer, pointed_thing,
			"zr_blueberry:sapling",
			-- minp, maxp to be checked, relative to sapling pos
			{x = -1, y = 0, z = -1},
			{x = 1, y = 1, z = 1},
			-- maximum interval of interior volume check
			2)

		return itemstack
	end,
})

minetest.register_lbm({
    name = "zr_blueberry:convert_saplings_to_blueberry_tree",
    nodenames = {"zr_blueberry:sapling"},
    action = function(pos)
        minetest.get_node_timer(pos):start(math.random(300, 1500))
    end
})
