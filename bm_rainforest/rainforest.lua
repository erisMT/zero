
minetest.register_biome({
	name = "bm_rainforest",
	node_top = "zr_dirt:litter",
	depth_top = 1,
	node_filler = "zr_dirt:dirt",
	depth_filler = 3,
	node_riverbed = "zr_sand:sand",
	depth_riverbed = 2,
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = "zr_stone:stair_cobble",
	y_max = 31000,
	y_min = 1,
	heat_point = 86,
	humidity_point = 65,
})

minetest.register_biome({
	name = "bm_rainforest_swamp",
	node_top = "zr_dirt:dirt",
	depth_top = 1,
	node_filler = "zr_dirt:dirt",
	depth_filler = 3,
	node_riverbed = "zr_sand:sand",
	depth_riverbed = 2,
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = "zr_stone:stair_cobble",
	y_max = 0,
	y_min = -1,
	heat_point = 86,
	humidity_point = 65,
})

minetest.register_biome({
	name = "bm_rainforest_ocean",
	node_top = "zr_sand:sand",
	depth_top = 1,
	node_filler = "zr_sand:sand",
	depth_filler = 3,
	node_riverbed = "zr_sand:sand",
	depth_riverbed = 2,
	node_cave_liquid = "zr_water:source",
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = "zr_stone:stair_cobble",
	vertical_blend = 1,
	y_max = -2,
	y_min = -255,
	heat_point = 86,
	humidity_point = 65,
})

local cave_liquid = {"zr_water:source"};

if(minetest.get_modpath("zr_lava")) ~= nil then
	cave_liquid = {"zr_water:source", "zr_lava:source"};
end

minetest.register_biome({
	name = "bm_rainforest_under",
	node_cave_liquid = cave_liquid,
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = "zr_stone:stair_cobble",
	y_max = -256,
	y_min = -31000,
	heat_point = 86,
	humidity_point = 65,
})

if(minetest.get_modpath("zr_grass")) ~= nil then
	zr_grass.add_jungle_to_biome("bm_rainforest")
	zr_grass.add_fern_to_biome("bm_rainforest")
end

if(minetest.get_modpath("zr_jungle")) ~= nil then
	zr_jungle.add_to_biome("bm_rainforest")
	zr_jungle.add_emergent_to_biome("bm_rainforest")
	zr_jungle.add_log_to_biome("bm_rainforest")

	-- add additional trees at water level for swamp
	zr_jungle.add_to_biome("bm_rainforest_swamp", {
		name = "bm_rainforest:swamp_trees",
		place_on = {"zr_dirt:dirt"},
		sidelen = 16,
		noise_params = { 
			offset = 0.0, 
			scale = -0.1, 
			spread = {x = 200, y = 200, z = 200},
			seed = 354, 
			octaves = 1, 
			persist = 0.5 
		},
		y_max = 0,
		y_min = -1,
	})
end

if(minetest.get_modpath("zr_papyrus")) ~= nil then
	--zr_papyrus.add_to_biome_on_dirt("bm_rainforest_swamp")
end

if(minetest.get_modpath("zr_bug")) ~= nil then
	zr_bug.add_firefly_to_biome("bm_rainforest_swamp")
end

if (minetest.get_modpath("zr_coral") ~= nil) then
	zr_coral.add_to_biome("bm_rainforest_ocean")
end
