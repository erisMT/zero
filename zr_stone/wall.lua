
local S = minetest.get_translator("zr_stone")

zr_fence.register_wall("zr_stone:cobble_wall", {
	description = S("Cobblestone Wall"),
	texture = "zr_stone_cobble.png",
	material = "zr_stone:cobble",
})
minetest.register_alias("stone:cobble_wall","zr_stone:cobble_wall")

zr_fence.register_wall("zr_stone:mossycobble_wall", {
	description = S("Mossy Cobblestone Wall"),
	texture = "zr_stone_mossycobble.png",
	material = "zr_stone:mossycobble",
})
minetest.register_alias("stone:mossycobble_wall","zr_stone:mossycobble_wall")
