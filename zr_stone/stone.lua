
local S = minetest.get_translator("zr_stone")

zr_stone.sounds = {
    footstep = {name = "zr_stone_footstep", gain = 0.3},
    dig = {name = "zr_stone_dig", gain = 0.5},
    dug = {name = "zr_stone_footstep", gain = 1.0},
    place = {name = "zr_stone_place", gain = 1.0},
}

-- STONE
minetest.register_node("zr_stone:stone", {
    description = S("Stone"),
    tiles = {"zr_stone.png"},
    groups = {cracky = 3, stone = 1},
    drop = "zr_stone:cobble",
    legacy_mineral = true,
    sounds = zr_stone.sounds,
})
minetest.register_alias("stone:stone","zr_stone:stone")
minetest.register_alias("stone","zr_stone:stone")

-- COBBLE 
minetest.register_node("zr_stone:cobble", {
    description = S("Cobblestone"),
    tiles = {"zr_stone_cobble.png"},
    is_ground_content = false,
    groups = {cracky = 3, stone = 2, blast_loss = 3},
    sounds = zr_stone.sounds,
})
minetest.register_alias("stone:cobble","zr_stone:cobble")
minetest.register_alias("cobble","zr_stone:cobble")

minetest.register_craft({
	type = "cooking",
	output = "zr_stone:stone",
	recipe = "zr_stone:cobble",
})

-- MOSSY COBBLE
minetest.register_node("zr_stone:mossycobble", {
	description = S("Mossy Cobblestone"),
	tiles = {"zr_stone_mossycobble.png"},
	is_ground_content = false,
	groups = {cracky = 3, stone = 1},
	sounds = zr_stone.sounds,
})
minetest.register_alias("stone:mossycobble","zr_stone:mossycobble")
minetest.register_alias("mossycobble","zr_stone:mossycobble")

minetest.register_craft({
	type = "cooking",
	output = "zr_stone:stone",
	recipe = "zr_stone:mossycobble",
})

-- STONE BRICK
minetest.register_node("zr_stone:brick", {
	description = S("Stone Brick"),
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {"zr_stone_brick.png"},
	is_ground_content = false,
	groups = {cracky = 2, stone = 1},
	sounds = zr_stone.sounds,
})
minetest.register_alias("stone:brick","zr_stone:brick")

minetest.register_craft({
	output = "zr_stone:brick 4",
	recipe = {
		{"zr_stone:stone", "zr_stone:stone"},
		{"zr_stone:stone", "zr_stone:stone"},
	}
})

-- STONE BLOCK
minetest.register_node("zr_stone:block", {
	description = S("Stone Block"),
	tiles = {"zr_stone_block.png"},
	is_ground_content = false,
	groups = {cracky = 2, stone = 1},
	sounds = zr_stone.sounds,
})
minetest.register_alias("stone:block","zr_stone:block")

minetest.register_craft({
	output = "zr_stone:block 9",
	recipe = {
		{"zr_stone:stone", "zr_stone:stone", "zr_stone:stone"},
		{"zr_stone:stone", "zr_stone:stone", "zr_stone:stone"},
		{"zr_stone:stone", "zr_stone:stone", "zr_stone:stone"},
	}
})

