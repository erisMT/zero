
-- Desert
local dungeon_stair = "zr_stone:cobble"
local desert_dungeon_stair = "zr_sand:desert_sandstone"

if(minetest.get_modpath("zr_stair")) ~= nil then
	sand_dungeon_stair = "zr_sand:stair_desert_sandstone"
	dungeon_stair = "zr_stone:stair_cobble"
end

local cave_liquid = {"zr_water:source"};
if(minetest.get_modpath("zr_lava")) ~= nil then
	cave_liquid = {"zr_water:source", "zr_lava:source"};
end

minetest.register_biome({
	name = "bm_desert",
	node_top = "zr_sand:desert",
	depth_top = 1,
	node_filler = "zr_sand:desert",
	depth_filler = 1,
	node_stone = "zr_sand:desert_sandstone",
	node_riverbed = "zr_sand:sand",
	depth_riverbed = 2,
	node_dungeon = "zr_sand:desert_sandstone",
	node_dungeon_stair = sand_dungeon_stair,
	y_max = 31000,
	y_min = 4,
	heat_point = 92,
	humidity_point = 16,
})

minetest.register_biome({
	name = "bm_desert_ocean",
	node_top = "zr_sand:sand",
	depth_top = 1,
	node_filler = "zr_sand:sand",
	depth_filler = 3,
	node_stone = "zr_sand:desert_sandstone",
	node_riverbed = "zr_sand:sand",
	depth_riverbed = 2,
	node_cave_liquid = "zr_water:source",
	node_dungeon = "zr_sand:desert_sandstone",
	node_dungeon_stair = sand_dungeon_stair,
	vertical_blend = 1,
	y_max = 3,
	y_min = -255,
	heat_point = 92,
	humidity_point = 16,
})

minetest.register_biome({
	name = "bm_desert_under",
	node_cave_liquid = cave_liquid,
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = dungeon_stair,
	y_max = -256,
	y_min = -31000,
	heat_point = 92,
	humidity_point = 16,
})

if(minetest.get_modpath("zr_stair")) ~= nil then
	sand_dungeon_stair = "zr_sand:stair_sandstone_block"
end
	-- Sandstone desert

minetest.register_biome({
	name = "bm_sandstone_desert",
	node_top = "zr_sand:sand",
	depth_top = 1,
	node_filler = "zr_sand:sand",
	depth_filler = 1,
	node_stone = "zr_sand:sandstone",
	node_riverbed = "zr_sand:sand",
	depth_riverbed = 2,
	node_dungeon = "zr_sand:sandstonebrick",
	node_dungeon_stair = sand_dungeon_stair,
	y_max = 31000,
	y_min = 4,
	heat_point = 60,
	humidity_point = 0,
})

minetest.register_biome({
	name = "bm_sandstone_desert_ocean",
	node_top = "zr_sand:sand",
	depth_top = 1,
	node_filler = "zr_sand:sand",
	depth_filler = 3,
	node_stone = "zr_sand:sandstone",
	node_riverbed = "zr_sand:sand",
	depth_riverbed = 2,
	node_cave_liquid = "zr_water:source",
	node_dungeon = "zr_sand:sandstonebrick",
	node_dungeon_stair = sand_dungeon_stair,
	y_max = 3,
	y_min = -255,
	heat_point = 60,
	humidity_point = 0,
})

minetest.register_biome({
	name = "bm_sandstone_desert_under",
	node_cave_liquid = cave_liquid,
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = dungeon_stair,
	y_max = -256,
	y_min = -31000,
	heat_point = 60,
	humidity_point = 0,
})

-- Cold desert
minetest.register_biome({
	name = "bm_cold_desert",
	node_top = "zr_sand:silver",
	depth_top = 1,
	node_filler = "zr_sand:silver",
	depth_filler = 1,
	node_riverbed = "zr_sand:sand",
	depth_riverbed = 2,
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = dungeon_stair,
	y_max = 31000,
	y_min = 4,
	heat_point = 40,
	humidity_point = 0,
})

minetest.register_biome({
	name = "bm_cold_desert_ocean",
	node_top = "zr_sand:sand",
	depth_top = 1,
	node_filler = "zr_sand:sand",
	depth_filler = 3,
	node_riverbed = "zr_sand:sand",
	depth_riverbed = 2,
	node_cave_liquid = "zr_water:source",
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = dungeon_stair,
	vertical_blend = 1,
	y_max = 3,
	y_min = -255,
	heat_point = 40,
	humidity_point = 0,
})

minetest.register_biome({
	name = "bm_cold_desert_under",
	node_cave_liquid = cave_liquid,
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = dungeon_stair,
	y_max = -256,
	y_min = -31000,
	heat_point = 40,
	humidity_point = 0,
})

if (minetest.get_modpath("zr_coral") ~= nil) then
	zr_coral.add_to_biome("bm_desert_ocean")
end

if(minetest.get_modpath("zr_kelp")) ~= nil then
	zr_kelp.add_to_biome("bm_cold_desert_ocean")
end

if(minetest.get_modpath("zr_cactus")) ~= nil then
	zr_cactus.add_all_to_biome("bm_desert")
end

if(minetest.get_modpath("zr_shrub")) ~= nil then
	zr_shrub.add_to_biome("bm_desert", {
		biomes = {"bm_desert", "bm_cold_desert", "bm_sandstone_desert"}
	})
end
