
local S = minetest.get_translator("zr_kelp")

zr_kelp.sounds = {}
for k, v in pairs(zr_sand.sounds) do zr_kelp.sounds[k] = v end
zr_kelp.sounds.dig = {name = "zr_tools_dig_snappy", gain = 0.2}
zr_kelp.sounds.dug = {name = "zr_dirt_grass_footstep", gain = 0.25}

minetest.register_node("zr_kelp:kelp", {
	description = S("Kelp"),
	drawtype = "plantlike_rooted",
	waving = 1,
	tiles = {"zr_sand.png"},
	special_tiles = {{name = "zr_kelp.png", tileable_vertical = true}},
	inventory_image = "zr_kelp.png",
	wield_image = "zr_kelp.png",
	paramtype = "light",
	paramtype2 = "leveled",
	groups = {snappy = 3, oddly_breakable_by_hand=3},
	selection_box = {
		type = "fixed",
		fixed = {
				{-0.5, -0.5, -0.5, 0.5, 0.5, 0.5},
				{-2/16, 0.5, -2/16, 2/16, 3.5, 2/16},
		},
	},
	node_dig_prediction = "zr_sand:sand",
	node_placement_prediction = "",
	sounds = zr_kelp.sounds,

	on_place = function(itemstack, placer, pointed_thing)
		-- Call on_rightclick if the pointed node defines it
		if pointed_thing.type == "node" and placer and
				not placer:get_player_control().sneak then
			local node_ptu = minetest.get_node(pointed_thing.under)
			local def_ptu = minetest.registered_nodes[node_ptu.name]
			if def_ptu and def_ptu.on_rightclick then
				return def_ptu.on_rightclick(pointed_thing.under, node_ptu, placer,
					itemstack, pointed_thing)
			end
		end

		local pos = pointed_thing.under
		if minetest.get_node(pos).name ~= "zr_sand:sand" then
			return itemstack
		end

		local height = math.random(4, 6)
		local pos_top = {x = pos.x, y = pos.y + height, z = pos.z}
		local node_top = minetest.get_node(pos_top)
		local def_top = minetest.registered_nodes[node_top.name]
		local player_name = placer:get_player_name()

		if def_top and def_top.liquidtype == "source" and
				minetest.get_item_group(node_top.name, "water") > 0 then
			if not minetest.is_protected(pos, player_name) and
					not minetest.is_protected(pos_top, player_name) then
				minetest.set_node(pos, {name = "zr_kelp:kelp",
					param2 = height * 16})
				if not minetest.is_creative_enabled(player_name) then
					itemstack:take_item()
				end
			else
				minetest.chat_send_player(player_name, "Node is protected")
				minetest.record_protection_violation(pos, player_name)
			end
		end

		return itemstack
	end,

	after_destruct  = function(pos, oldnode)
		minetest.set_node(pos, {name = "zr_sand:sand"})
	end
})
minetest.register_alias("kelp:kelp", "zr_kelp:kelp")
minetest.register_alias("kelp", "zr_kelp:kelp")
