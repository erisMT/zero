
zr_bug = {};

local mod_path = minetest.get_modpath("zr_bug")

dofile(mod_path.."/firefly.lua")

if minetest.get_modpath("zr_flower") ~= nil then
	dofile(mod_path.."/butterfly.lua")
end
if minetest.get_modpath("zr_wool") ~= nil then
	dofile(mod_path.."/net.lua")
end
if minetest.get_modpath("zr_glass") ~= nil and minetest.get_modpath("zr_vessel") ~= nil then
	dofile(mod_path.."/bottle.lua")
end
