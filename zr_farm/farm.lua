local S = minetest.get_translator("zr_farm")

-- Wheat

zr_farm.register_plant("zr_farm:wheat", {
	description = S("Wheat Seed"),
	harvest_description = S("Wheat"),
	paramtype2 = "meshoptions",
	inventory_image = "zr_farm_wheat_seed.png",
	steps = 8,
	minlight = 13,
	groups = {food_wheat = 1, flammable = 4},
	place_param2 = 3,
})
minetest.register_alias("farm:wheat", "zr_farm:wheat") 
minetest.register_alias("farm:seed_wheat", "zr_farm:seed_wheat") 
minetest.register_alias("wheat", "zr_farm:wheat") 
minetest.register_alias("seed_wheat", "zr_farm:seed_wheat") 

minetest.register_craftitem("zr_farm:flour", {
	description = S("Flour"),
	inventory_image = "zr_farm_flour.png",
	groups = {food_flour = 1, flammable = 1},
})
minetest.register_alias("farm:flour", "zr_farm:flour") 
minetest.register_alias("flour", "zr_farm:flour") 

minetest.register_craftitem("zr_farm:bread", {
	description = S("Bread"),
	inventory_image = "zr_farm_bread.png",
	on_use = minetest.item_eat(5),
	groups = {food_bread = 1, flammable = 2},
})
minetest.register_alias("farm:bread", "zr_farm:bread") 
minetest.register_alias("bread", "zr_farm:bread") 

minetest.register_craft({
	type = "shapeless",
	output = "zr_farm:flour",
	recipe = {"zr_farm:wheat", "zr_farm:wheat", "zr_farm:wheat", "zr_farm:wheat"}
})

minetest.register_craft({
	type = "cooking",
	cooktime = 15,
	output = "zr_farm:bread",
	recipe = "zr_farm:flour"
})


-- Cotton

zr_farm.register_plant("zr_farm:cotton", {
	description = S("Cotton Seed"),
	harvest_description = S("Cotton"),
	inventory_image = "zr_farm_cotton_seed.png",
	steps = 8,
	minlight = 13,
	groups = {flammable = 4},
})
minetest.register_alias("farm:cotton", "zr_farm:cotton") 
minetest.register_alias("farm:seed_cotton", "zr_farm:seed_cotton") 
minetest.register_alias("cotton", "zr_farm:cotton") 
minetest.register_alias("seed_cotton", "zr_farm:seed_cotton") 

function zr_farm.add_cotton_to_biome(biome, dirt)
	minetest.register_decoration({
		name = biome..":cotton_wild",
		deco_type = "simple",
		place_on = {dirt},
		sidelen = 16,
		noise_params = {
			offset = -0.1,
			scale = 0.1,
			spread = {x = 50, y = 50, z = 50},
			seed = 4242,
			octaves = 3,
			persist = 0.7
		},
		biomes = {biome},
		y_max = 31000,
		y_min = 1,
		decoration = "zr_farm:cotton_wild",
	})
end

minetest.register_craft({
	output = "zr_wool:white",
	recipe = {
		{"zr_farm:cotton", "zr_farm:cotton"},
		{"zr_farm:cotton", "zr_farm:cotton"},
	}
})

minetest.register_craft({
	output = "zr_wool:string 2",
	recipe = {
		{"zr_farm:cotton"},
		{"zr_farm:cotton"},
	}
})


-- Straw

minetest.register_craft({
	output = "zr_farm:straw 3",
	recipe = {
		{"zr_farm:wheat", "zr_farm:wheat", "zr_farm:wheat"},
		{"zr_farm:wheat", "zr_farm:wheat", "zr_farm:wheat"},
		{"zr_farm:wheat", "zr_farm:wheat", "zr_farm:wheat"},
	}
})

minetest.register_craft({
	output = "zr_farm:wheat 3",
	recipe = {
		{"zr_farm:straw"},
	}
})


-- Fuels

minetest.register_craft({
	type = "fuel",
	recipe = "zr_farm:wheat",
	burntime = 1,
})

minetest.register_craft({
	type = "fuel",
	recipe = "zr_farm:cotton",
	burntime = 1,
})


minetest.register_craft({
	type = "fuel",
	recipe = "zr_farm:hoe_wood",
	burntime = 5,
})


-- Register farming items as dungeon loot

if minetest.global_exists("zr_dungeon_loot") then
	zr_dungeon_loot.register({
		{name = "zr_farm:wheat", chance = 0.5, count = {2, 5}},
		{name = "zr_farm:seed_cotton", chance = 0.4, count = {1, 4},
			types = {"normal"}},
	})
end
