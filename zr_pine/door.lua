local S = minetest.get_translator("zr_pine");

-- GATE
zr_door.register_gate("zr_pine:gate", {
	description = S("Pine Wood Fence Gate"),
	texture = "zr_pine_wood.png",
	material = "zr_pine:wood",
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
	sounds = zr_wood.sounds,
})
minetest.register_alias("pine:gate", "zr_pine:gate")
