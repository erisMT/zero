
local S = minetest.get_translator("zr_wood")

zr_tools.register_pickaxe("zr_wood:pick", {
    description = S("Wooden Pickaxe"),
    inventory_image = "zr_wood_pick.png",
    tool_capabilities = {
        full_punch_interval = 1.2,
        max_drop_level=0,
        groupcaps={
			cracky = {times={[3]=1.60}, uses=10, maxlevel=1},
        },
        damage_groups = {fleshy=2},
    },
	groups = {pickaxe = 1, flammable = 2},
	recipeitem = "group:wood",
})
minetest.register_alias("wood:pick", "zr_wood:pick")

zr_tools.register_axe("zr_wood:axe", {
    description = S("Wooden Axe"),
    inventory_image = "zr_wood_axe.png",
    tool_capabilities = {
        full_punch_interval = 1.0,
        max_drop_level=0,
        groupcaps={
			choppy = {times={[2]=3.00, [3]=1.60}, uses=10, maxlevel=1},
        },
        damage_groups = {fleshy=2},
    },
	groups = {axe = 1, flammable = 2},
	
	recipeitem = "group:wood"
})
minetest.register_alias("wood:axe", "zr_wood:axe")

zr_tools.register_shovel("zr_wood:shovel", {
    description = S("Wooden Shovel"),
    inventory_image = "zr_wood_shovel.png",
    tool_capabilities = {
        full_punch_interval = 1.2,
        max_drop_level=0,
        groupcaps={
			crumbly = {times={[1]=3.00, [2]=1.60, [3]=0.60}, uses=10, maxlevel=1},
        },
        damage_groups = {fleshy=2},
    },
	groups = {shovel = 1, flammable = 2},
	recipeitem = "group:wood"
})
minetest.register_alias("wood:shovel", "zr_wood:shovel")

zr_tools.register_sword("zr_wood:sword", {
    description = S("Wooden Sword"),
    inventory_image = "zr_wood_sword.png",
    tool_capabilities = {
        full_punch_interval = 1,
        max_drop_level=0,
        groupcaps={
			snappy={times={[2]=1.6, [3]=0.40}, uses=10, maxlevel=1},
        },
        damage_groups = {fleshy=2},
    },
	groups = {sword = 1, flammable = 2},
	recipeitem = "group:wood"
})
minetest.register_alias("wood:sword", "zr_wood:sword")
