local S = minetest.get_translator("zr_diamond");

minetest.register_node("zr_diamond:ore", {
	description = S("Diamond Ore"),
	tiles = {"zr_stone.png^zr_diamond_mineral.png"},
	groups = {cracky = 1},
	drop = "zr_diamond:diamond",
	sounds = zr_stone.sounds,
})
minetest.register_alias("diamond:ore", "zr_diamond:ore")

minetest.register_node("zr_diamond:block", {
	description = S("Diamond Block"),
	tiles = {"zr_diamond_block.png"},
	is_ground_content = false,
	groups = {cracky = 1, level = 3},
	sounds = zr_stone.sounds,
})
minetest.register_alias("diamond:block", "zr_diamond:block")

-- crafting
minetest.register_craftitem("zr_diamond:diamond", {
	description = S("Diamond"),
	inventory_image = "zr_diamond.png",
})
minetest.register_alias("diamond:diamond", "zr_diamond:diamond")
minetest.register_alias("diamond", "zr_diamond:diamond")

minetest.register_craft({
	output = "zr_diamond:diamond 9",
	recipe = {
		{"zr_diamond:block"},
	}
})

minetest.register_craft({
	output = "zr_diamond:block",
	recipe = {
		{"zr_diamond:diamond", "zr_diamond:diamond", "zr_diamond:diamond"},
		{"zr_diamond:diamond", "zr_diamond:diamond", "zr_diamond:diamond"},
		{"zr_diamond:diamond", "zr_diamond:diamond", "zr_diamond:diamond"},
	}
})

-- ore distribution
minetest.register_ore({
	ore_type       = "scatter",
	ore            = "zr_diamond:ore",
	wherein        = "mapgen_stone",
	clust_scarcity = 15 * 15 * 15,
	clust_num_ores = 4,
	clust_size     = 3,
	y_max          = 31000,
	y_min          = 1025,
})
minetest.register_ore({
	ore_type       = "scatter",
	ore            = "zr_diamond:ore",
	wherein        = "mapgen_stone",
	clust_scarcity = 17 * 17 * 17,
	clust_num_ores = 4,
	clust_size     = 3,
	y_max          = -1024,
	y_min          = -2047,
})
minetest.register_ore({
	ore_type       = "scatter",
	ore            = "zr_diamond:ore",
	wherein        = "mapgen_stone",
	clust_scarcity = 15 * 15 * 15,
	clust_num_ores = 4,
	clust_size     = 3,
	y_max          = -2048,
	y_min          = -31000,
})
