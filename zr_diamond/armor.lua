
local S = minetest.get_translator("zr_diamond");

-- Boots
simple_armor.register("zr_diamond:boots", {
	description = S("Diamond Boots"),
	inventory_image = "zr_diamond_boots_item.png",
	armor = {
		slot = "feet",
		groups = { fleshy = 15 }, 
		texture = "zr_diamond_boots.png",
	},
})
simple_armor.boots_recipe("zr_diamond:boots", "zr_diamond:diamond")
minetest.register_alias("diamond:boots", "zr_diamond:boots")

-- Chestplate
simple_armor.register("zr_diamond:chestplate", {
	description = S("Diamond Boots"),
	inventory_image = "zr_diamond_chestplate_item.png",
	armor = {
		slot = "torso",
		groups = { fleshy = 20 }, 
		texture = "zr_diamond_chestplate.png",
	},
})
simple_armor.chestplate_recipe("zr_diamond:chestplate", "zr_diamond:diamond")
minetest.register_alias("diamond:chestplate", "zr_diamond:chestplate")

-- Leggings
simple_armor.register("zr_diamond:leggings", {
	description = S("Diamond Boots"),
	inventory_image = "zr_diamond_leggings_item.png",
	armor = {
		slot = "legs",
		groups = { fleshy = 20 }, 
		texture = "zr_diamond_leggings.png",
	},
})
simple_armor.leggings_recipe("zr_diamond:leggings", "zr_diamond:diamond")
minetest.register_alias("diamond:leggings", "zr_diamond:leggings")

-- Helmet
simple_armor.register("zr_diamond:helmet", {
	description = S("Diamond Boots"),
	inventory_image = "zr_diamond_helmet_item.png",
	armor = {
		slot = "head",
		groups = { fleshy = 15 }, 
		texture = "zr_diamond_helmet.png",
	},
})
simple_armor.helmet_recipe("zr_diamond:helmet", "zr_diamond:diamond")
minetest.register_alias("diamond:helmet", "zr_diamond:helmet")

