
zr_diamond = {}

local mod_path = minetest.get_modpath("zr_diamond")
dofile(mod_path.."/diamond.lua")

if (minetest.get_modpath("zr_tools") ~= nil) then
	dofile(mod_path.."/tools.lua")
end

if (minetest.get_modpath("simple_armor") ~= nil) then
	dofile(mod_path.."/armor.lua")
end
