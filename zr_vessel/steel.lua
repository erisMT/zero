local S = minetest.get_translator("zr_vessel")

minetest.register_node("zr_vessel:steel_bottle", {
	description = S("Empty Heavy Steel Bottle"),
	drawtype = "plantlike",
	tiles = {"zr_vessel_steel_bottle.png"},
	inventory_image = "zr_vessel_steel_bottle.png",
	wield_image = "zr_vessel_steel_bottle.png",
	paramtype = "light",
	is_ground_content = false,
	walkable = false,
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.3, 0.25}
	},
	groups = {vessel = 1, dig_immediate = 3, attached_node = 1},
	sounds = zr_metal.sounds,
})
minetest.register_alias("vessel:steel_bottle", "zr_vessel:steel_bottle")

minetest.register_craft( {
	output = "zr_vessel:steel_bottle 5",
	recipe = {
		{"zr_iron:ingot", "", "zr_iron:ingot"},
		{"zr_iron:ingot", "", "zr_iron:ingot"},
		{"", "zr_iron:ingot", ""}
	}
})

minetest.register_craft( {
	type = "cooking",
	output = "zr_iron:ingot",
	recipe = "zr_vessel:steel_bottle",
})

