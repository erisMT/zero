
zr_vessel = {};

local mod_path = minetest.get_modpath("zr_vessel")

dofile(mod_path.."/glass.lua")
if minetest.get_modpath("zr_wood") ~= nil then
	dofile(mod_path.."/shelf.lua")
end
if minetest.get_modpath("zr_iron") ~= nil then
	dofile(mod_path.."/steel.lua")
end
