
local S = minetest.get_translator("zr_tin");

local tin_metal = zr_metal.register_metal("zr_tin", {
	block = {description = S("Tin Block"), png = "zr_tin_block.png"},
	ingot = {description = S("Tin Ingot"), png = "zr_tin_ingot.png"},
	lump =  {description = S("Tin Lump"), png = "zr_tin_lump.png"},
	ore =   {description = S("Tin Ore"), png = "zr_tin_mineral.png"},
})
minetest.register_alias("tin:block", "zr_tin:block")
minetest.register_alias("tin:ingot", "zr_tin:ingot")
minetest.register_alias("tin:lump", "zr_tin:lump")
minetest.register_alias("tin:ore", "zr_tin:ore")

if (tin_metal.ore ~= nil) then
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = tin_metal.ore,
		wherein        = "mapgen_stone",
		clust_scarcity = 10 * 10 * 10,
		clust_num_ores = 5,
		clust_size     = 3,
		y_max          = 31000,
		y_min          = 1025,
	})
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = tin_metal.ore,
		wherein        = "mapgen_stone",
		clust_scarcity = 13 * 13 * 13,
		clust_num_ores = 4,
		clust_size     = 3,
		y_max          = -64,
		y_min          = -127,
	})
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = tin_metal.ore,
		wherein        = "mapgen_stone",
		clust_scarcity = 10 * 10 * 10,
		clust_num_ores = 5,
		clust_size     = 3,
		y_max          = -128,
		y_min          = -31000,
	})
end
