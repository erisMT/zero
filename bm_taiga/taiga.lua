
minetest.register_biome({
	name = "bm_taiga",
	node_dust = "zr_snow:snow",
	node_top = "zr_dirt:snow",
	depth_top = 1,
	node_filler = "zr_dirt:dirt",
	depth_filler = 3,
	node_riverbed = "zr_sand:sand",
	depth_riverbed = 2,
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = "zr_stone:stair_cobble",
	y_max = 31000,
	y_min = 4,
	heat_point = 25,
	humidity_point = 70,
})

minetest.register_biome({
	name = "bm_taiga_ocean",
	node_dust = "zr_snow:snow",
	node_top = "zr_sand:sand",
	depth_top = 1,
	node_filler = "zr_sand:sand",
	depth_filler = 3,
	node_riverbed = "zr_sand:sand",
	depth_riverbed = 2,
	node_cave_liquid = "zr_water:source",
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = "zr_stone:stair_cobble",
	vertical_blend = 1,
	y_max = 3,
	y_min = -255,
	heat_point = 25,
	humidity_point = 70,
})

local cave_liquid = {"zr_water:source"};
if(minetest.get_modpath("zr_lava")) ~= nil then
	cave_liquid = {"zr_water:source", "zr_lava:source"};
end

minetest.register_biome({
	name = "bm_taiga_under",
	node_cave_liquid = cave_liquid,
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = "zr_stone:stair_cobble",
	y_max = -256,
	y_min = -31000,
	heat_point = 25,
	humidity_point = 70,
})

if(minetest.get_modpath("zr_pine")) ~= nil then
	zr_pine.add_to_biome("bm_taiga")
	zr_pine.add_small_to_biome("bm_taiga")
	zr_pine.add_log_to_biome("bm_taiga")
end

if(minetest.get_modpath("zr_kelp")) ~= nil then
	zr_kelp.add_to_biome("bm_taiga_ocean")
end
	
