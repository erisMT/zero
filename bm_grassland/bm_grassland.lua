minetest.register_biome({
	name = "bm_grassland",
	node_top = "zr_dirt:grass",
	depth_top = 1, 
	node_filler = "zr_dirt:dirt",
	depth_filler = 1, 
	node_riverbed = "zr_sand:sand",
	depth_riverbed = 2, 
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = "zr_stone:stair_stone",
	y_max = 31000,
	y_min = 6, 
	heat_point = 50,
	humidity_point = 35,
})   

minetest.register_biome({
	name = "bm_grassland_dunes",
	node_top = "zr_sand:sand",
	depth_top = 1, 
	node_filler = "zr_sand:sand",
	depth_filler = 2, 
	node_riverbed = "zr_sand:sand",
	depth_riverbed = 2, 
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = "zr_stone:stair_cobble",
	vertical_blend = 1, 
	y_max = 5, 
	y_min = 4, 
	heat_point = 50,
	humidity_point = 35,
})   

minetest.register_biome({
	name = "bm_grassland_ocean",
	node_top = "zr_sand:sand",
	depth_top = 1, 
	node_filler = "zr_sand:sand",
	depth_filler = 3, 
	node_riverbed = "zr_sand:sand",
	depth_riverbed = 2, 
	node_cave_liquid = "zr_water:source",
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = "zr_stone:stair_cobble",
	y_max = 3, 
	y_min = -255,
	heat_point = 50,
	humidity_point = 35,
})   

local cave_liquid = {"zr_water:source"};

if(minetest.get_modpath("zr_lava")) ~= nil then
	cave_liquid = {"zr_water:source", "zr_lava:source"};
end

minetest.register_biome({
	name = "bm_grassland_under",
	node_cave_liquid = cave_liquid,
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = "zr_stone:stair_cobble",
	y_max = -256,
	y_min = -31000,
	heat_point = 50,
	humidity_point = 35,
})   

if(minetest.get_modpath("zr_grass")) ~= nil then
	zr_grass.add_to_biome("bm_grassland")
end

if(minetest.get_modpath("zr_flower")) ~= nil then
	zr_flower.add_all_flowers_to_biome("bm_grassland")
end

if(minetest.get_modpath("zr_bug")) ~= nil then
	zr_bug.add_butterfly_to_biome("bm_grassland")
	zr_bug.add_firefly_to_biome("bm_grassland")
end

if(minetest.get_modpath("zr_papyrus")) ~= nil then
	zr_papyrus.add_to_biome_on_dirt("bm_grassland_ocean")
end

if(minetest.get_modpath("zr_kelp")) ~= nil then
	zr_kelp.add_to_biome("bm_grassland_ocean")
end

if(minetest.get_modpath("zr_blueberry")) ~= nil then
	zr_blueberry.add_to_biome("bm_grassland")
end

