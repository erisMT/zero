
zr_paper = {};

local mod_path = minetest.get_modpath("zr_paper")

dofile(mod_path.."/paper.lua")
dofile(mod_path.."/book.lua")
dofile(mod_path.."/bookshelf.lua")
