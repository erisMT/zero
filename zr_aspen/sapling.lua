local S = minetest.get_translator("zr_aspen");

local function start_timer(pos)
	minetest.get_node_timer(pos):start(math.random(300, 1500))
end

function zr_aspen.grow_aspen_sapling(pos) 

	if not zr_wood.check_grow_tree(pos) then
		start_timer(pos)
		return
	end

    local tree_schema = minetest.get_modpath("zr_aspen") .. "/schematics/zr_aspen_tree_from_sapling.mts"
    minetest.place_schematic({x=pos.x-4, y=pos.y-1, z=pos.z-4}, tree_schema, "random", nil, false)
end

minetest.register_node("zr_aspen:sapling", {
	description = S("Aspen Tree Sapling"),
    drawtype = "plantlike",
    tiles = {"zr_aspen_sapling.png"},
    inventory_image = "zr_aspen_sapling.png",
    wield_image = "zr_aspen_sapling.png",
    paramtype = "light",
    sunlight_propagates = true,
    walkable = false,
    on_timer = zr_aspen.grow_aspen_sapling,
	selection_box = {
		type = "fixed",
		fixed = {-3 / 16, -0.5, -3 / 16, 3 / 16, 0.5, 3 / 16}
	},
    groups = {snappy = 2, dig_immediate = 3, flammable = 3,
        attached_node = 1, sapling = 1},
    sounds = zr_wood.leaves_sounds,

    on_construct = start_timer,

    on_place = function(itemstack, placer, pointed_thing)
        itemstack = zr_wood.sapling_on_place(itemstack, placer, pointed_thing,
            "zr_aspen:sapling",
            -- minp_relative.y = 1 because sapling pos has been checked
			{x = -2, y = 1, z = -2},
			{x = 2, y = 12, z = 2},
            -- maximum interval of interior volume check
            4)

        return itemstack
    end,
})
minetest.register_alias("aspen:sapling", "zr_aspen:sapling")

minetest.register_lbm({
    name = "zr_aspen:convert_saplings_to_aspen_tree",
    nodenames = {"zr_aspen:sapling"},
    action = start_timer,
})

