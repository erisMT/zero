
local S = minetest.get_translator("zr_aspen")

minetest.register_node("zr_aspen:tree", {
	description = S("Aspen Tree"),
	tiles = {"zr_aspen_tree_top.png", "zr_aspen_tree_top.png", "zr_aspen_tree.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree = 1, choppy = 3, oddly_breakable_by_hand = 1, flammable = 3},
	sounds = zr_wood.sounds,
	on_place = minetest.rotate_node
})
minetest.register_alias("aspen:tree", "zr_aspen:tree")

minetest.register_node("zr_aspen:wood", {
	description = S("Aspen Wood Planks"),
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {"zr_aspen_wood.png"},
	is_ground_content = false,
	groups = {choppy = 3, oddly_breakable_by_hand = 2, flammable = 3, wood = 1},
	sounds = zr_wood.sounds,
})
minetest.register_alias("aspen:wood", "zr_aspen:wood")

minetest.register_node("zr_aspen:leaves", {
	description = S("Aspen Tree Leaves"),
	drawtype = "allfaces_optional",
	tiles = {"zr_aspen_leaves.png"},
--	special_tiles = {"zr_aspen_leaves_simple.png"},
	waving = 1,
	paramtype = "light",
	is_ground_content = false,
	groups = {snappy = 3, oddly_breakable_by_hand = 2, leafdecay = 3, flammable = 2, leaves = 1},
	drop = {
		max_items = 1,
		items = {
			{items = {"zr_aspen:sapling"}, rarity = 20},
			{items = {"zr_aspen:leaves"}}
		}
	},
	sounds = zr_wood.leaves_sounds,
	after_place_node = zr_wood.after_place_leaves,
})
minetest.register_alias("aspen:leaves", "zr_aspen:leaves")

zr_wood.register_leafdecay({
	trunks = {"zr_aspen:tree"},
	leaves = {"zr_aspen:leaves"},
	radius = 3,
})
