
local cave_liquid = {"zr_water:source"};
minetest.register_biome({
		name = "bm_savanna",
		node_top = "zr_dirt:grass_dry",
		depth_top = 1,
		node_filler = "zr_dirt:dry",
		depth_filler = 1,
		node_riverbed = "zr_sand:sand",
		depth_riverbed = 2,
		node_dungeon = "zr_stone:cobble",
		node_dungeon_alt = "zr_stone:mossycobble",
		node_dungeon_stair = "zr_stone:stair_cobble",
		y_max = 31000,
		y_min = 1,
		heat_point = 89,
		humidity_point = 42,
	})

	minetest.register_biome({
		name = "bm_savanna_shore",
		node_top = "zr_dirt:dry",
		depth_top = 1,
		node_filler = "zr_dirt:dry",
		depth_filler = 3,
		node_riverbed = "zr_sand:sand",
		depth_riverbed = 2,
		node_dungeon = "zr_stone:cobble",
		node_dungeon_alt = "zr_stone:mossycobble",
		node_dungeon_stair = "zr_stone:stair_cobble",
		y_max = 0,
		y_min = -1,
		heat_point = 89,
		humidity_point = 42,
	})

	minetest.register_biome({
		name = "bm_savanna_ocean",
		node_top = "zr_sand:sand",
		depth_top = 1,
		node_filler = "zr_sand:sand",
		depth_filler = 3,
		node_riverbed = "zr_sand:sand",
		depth_riverbed = 2,
		node_cave_liquid = cave_liquid,
		node_dungeon = "zr_stone:cobble",
		node_dungeon_alt = "zr_stone:mossycobble",
		node_dungeon_stair = "zr_stone:stair_cobble",
		vertical_blend = 1,
		y_max = -2,
		y_min = -255,
		heat_point = 89,
		humidity_point = 42,
	})

if(minetest.get_modpath("zr_lava")) ~= nil then
	cave_liquid = {"zr_water:source", "zr_lava:source"};
end

minetest.register_biome({
	name = "bm_savanna_under",
	node_cave_liquid = cave_liquid,
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = "zr_stairs:stair_cobble",
	y_max = -256,
	y_min = -31000,
	heat_point = 89,
	humidity_point = 42,
})


if(minetest.get_modpath("zr_papyrus")) ~= nil then
	zr_papyrus.add_to_biome_on_dirt("zr_grassland_ocean")
end

if(minetest.get_modpath("zr_grass")) ~= nil then
	zr_grass.add_dry_to_biome("bm_savanna")
end

if(minetest.get_modpath("zr_acacia")) ~= nil then
	zr_acacia.add_to_biome("bm_savanna")
	zr_acacia.add_log_to_biome("bm_savanna")
end

if minetest.get_modpath("zr_papyrus") then
	zr_papyrus.add_to_biome_on_dry_dirt("bm_savanna_ocean")
end

if (minetest.get_modpath("zr_coral") ~= nil) then
	zr_coral.add_to_biome("bm_savanna_ocean")
end
