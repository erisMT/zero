
zr_jungle = {};

local mod_path = minetest.get_modpath("zr_jungle")

dofile(mod_path.."/jungle.lua")
dofile(mod_path.."/sapling.lua")
dofile(mod_path.."/mapgen.lua")

if minetest.get_modpath("zr_fence") ~= nil then
	dofile(mod_path.."/fence.lua")
end
if (minetest.get_modpath("zr_stair") ~= nil) then
	dofile(mod_path.."/stair.lua")
end
if (minetest.get_modpath("zr_mese") ~= nil) then
	dofile(mod_path.."/meselamp.lua")
end
