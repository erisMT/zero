
local S = minetest.get_translator("zr_jungle");

-- FENCE
zr_fence.register_fence("zr_jungle:fence", {                                                                       
    description = S("Jungle Wood Fence"),
    texture = "zr_jungle_fence.png",
    material = "zr_jungle:wood",
    groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
    sounds = zr_wood.sounds,
})
minetest.register_alias("jungle:fence", "zr_jungle:fence")

zr_fence.register_fence_rail("zr_jungle:fence_rail", {                                                                       
    description = S("Jungle Wood Fence Rail"),
    texture = "zr_jungle_fence_rail.png",
    material = "zr_jungle:wood",
    groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
    sounds = zr_wood.sounds,
})
minetest.register_alias("jungle:fence_rail", "zr_jungle:fence_rail")
