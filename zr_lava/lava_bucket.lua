
local S = minetest.get_translator("zr_lava");

zr_bucket.register_liquid(
	"zr_lava:source",
	"zr_lava:flowing",
	"zr_lava:bucket",
	"zr_lava_bucket.png",
	S("Lava Bucket"),
	{tool = 1}
)
minetest.register_alias("lava:bucket","zr_lava:bucket")


minetest.register_craft({
	type = "fuel",
	recipe = "zr_lava:bucket",
	burntime = 60,
	replacements = {{"zr_lava:bucket", "zr_bucket:empty"}},
})

-- Register buckets as dungeon loot
if minetest.global_exists("zr_dungeon_loot") then
	zr_dungeon_loot.register({
		{name = "zr_lava:bucket", chance = 0.45, y = {-32768, -1},
			types = {"normal"}},
	})
end
