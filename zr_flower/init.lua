
zr_flower = {};

local mod_path = minetest.get_modpath("zr_flower")

dofile(mod_path.."/flower.lua")
dofile(mod_path.."/flowers.lua")
dofile(mod_path.."/lily.lua")
