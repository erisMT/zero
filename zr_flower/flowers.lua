local S = minetest.get_translator("zr_flower")

function zr_flower.add_all_flowers_to_biome(biome)
	local rarity = zr_flower.rarity
	zr_flower.add_to_biome("zr_flower:rose", biome, rarity.uncommon)
	zr_flower.add_to_biome("zr_flower:tulip", biome, rarity.common)
	zr_flower.add_to_biome("zr_flower:dandelion_yellow", biome, rarity.very_common)
	zr_flower.add_to_biome("zr_flower:dandelion_white", biome, rarity.very_common)
	zr_flower.add_to_biome("zr_flower:chrysanthemum", biome, rarity.uncommon)
	zr_flower.add_to_biome("zr_flower:geranium", biome, rarity.uncommon)
	zr_flower.add_to_biome("zr_flower:viola", biome, rarity.uncommon)
	zr_flower.add_to_biome("zr_flower:tulip_black", biome, rarity.rare)
end

zr_flower.register("zr_flower:rose", {
	description = S("Red Rose"),
	texture = "zr_flower_rose.png",
	selection_box = {type = "fixed",
		fixed = {-2 / 16, -0.5, -2 / 16, 2 / 16, 5 / 16, 2 / 16},
	},
	color = "red",
})
minetest.register_alias("flower:rose", "zr_flower:rose")

zr_flower.register("zr_flower:tulip", {
	description = S("Orange Tulip"),
	texture = "zr_flower_tulip.png",
	selection_box = {type = "fixed",
		fixed = {-2 / 16, -0.5, -2 / 16, 2 / 16, 5 / 16, 2 / 16},
	},
	color = "orange",
})
minetest.register_alias("flower:tulip", "zr_flower:tulip")

zr_flower.register("zr_flower:dandelion_yellow", {
	description = S("Yellow Dandelion"),
	texture = "zr_flower_dandelion_yellow.png",
	selection_box = {type = "fixed",
		fixed = {-4 / 16, -0.5, -4 / 16, 4 / 16, -2 / 16, 4 / 16},
	},
	color = "yellow",
})
minetest.register_alias("flower:dandelion_yellow", "zr_flower:dandelion_yellow")

zr_flower.register("zr_flower:dandelion_white", {
	description = S("White Dandelion"),
	texture = "zr_flower_dandelion_white.png",
	selection_box = {type = "fixed",
		fixed = {-5 / 16, -0.5, -5 / 16, 5 / 16, -2 / 16, 5 / 16},
	},
	color = "white",
})
minetest.register_alias("flower:dandelion_white", "zr_flower:dandelion_white")

zr_flower.register("zr_flower:chrysanthemum", {
	description = S("Green Chrysanthemum"),
	texture = "zr_flower_chrysanthemum.png",
	selection_box = {type = "fixed",
		fixed = {-4 / 16, -0.5, -4 / 16, 4 / 16, -1 / 16, 4 / 16},
	},
	color = "green",
})
minetest.register_alias("flower:chrysanthemum", "zr_flower:chrysanthemum")

zr_flower.register("zr_flower:geranium", {
	description = S("Blue Geranium"),
	texture = "zr_flower_geranium.png",
	selection_box = {type = "fixed",
		fixed = {-2 / 16, -0.5, -2 / 16, 2 / 16, 2 / 16, 2 / 16},
	},
	color = "blue",
})
minetest.register_alias("flower:geranium", "zr_flower:geranium")

zr_flower.register("zr_flower:viola", {
	description = S("Viola"),
	texture = "zr_flower_viola.png",
	selection_box = {type = "fixed",
		fixed = {-5 / 16, -0.5, -5 / 16, 5 / 16, -1 / 16, 5 / 16},
	},
	color = "violet",
})
minetest.register_alias("flower:viola", "zr_flower:viola")

zr_flower.register("zr_flower:tulip_black", {
	description = S("Black Tulip"),
	texture = "zr_flower_tulip_black.png",
	selection_box = {type = "fixed",
		fixed = {-2 / 16, -0.5, -2 / 16, 2 / 16, 3 / 16, 2 / 16},
	},
	color = "black",
})
minetest.register_alias("flower:tulip_black", "zr_flower:tulip_black")
