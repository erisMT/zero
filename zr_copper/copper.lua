-- Copper

local S = minetest.get_translator("zr_copper");

local copper_metal = zr_metal.register_metal("zr_copper", {
	block = {description = S("Copper Block"), png = "zr_copper_block.png"},
	ingot = {description = S("Copper Ingot"), png = "zr_copper_ingot.png"},
	lump =  {description = S("Copper Lump"), png = "zr_copper_lump.png"},
	ore =   {description = S("Copper Ore"), png = "zr_copper_mineral.png"},
})
minetest.register_alias("copper:block", "zr_copper:block")
minetest.register_alias("copper:ingot", "zr_copper:ingot")
minetest.register_alias("copper:lump", "zr_copper:lump")
minetest.register_alias("copper:ore", "zr_copper:ore")

if (copper_metal.ore ~= nil) then
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = copper_metal.ore,
		wherein        = "mapgen_stone",
		clust_scarcity = 9 * 9 * 9,
		clust_num_ores = 5,
		clust_size     = 3,
		y_max          = 31000,
		y_min          = 1025,
	})
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = copper_metal.ore,
		wherein        = "mapgen_stone",
		clust_scarcity = 12 * 12 * 12,
		clust_num_ores = 4,
		clust_size     = 3,
		y_max          = -64,
		y_min          = -127,
	})
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = copper_metal.ore,
		wherein        = "mapgen_stone",
		clust_scarcity = 9 * 9 * 9,
		clust_num_ores = 5,
		clust_size     = 3,
		y_max          = -128,
		y_min          = -31000,
	})
end
