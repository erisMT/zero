local world_path = minetest.get_worldpath()
local org_file = world_path .. "/zr_bed_spawns"
local file = world_path .. "/zr_bed_spawns"
local bkwd = false

-- check for PA's beds mod spawns
local cf = io.open(world_path .. "/zr_bed_player_spawns", "r")
if cf ~= nil then
	io.close(cf)
	file = world_path .. "/zr_bed_player_spawns"
	bkwd = true
end

function zr_bed.read_spawns()
	local spawns = zr_bed.spawn
	local input = io.open(file, "r")
	if input and not bkwd then
		repeat
			local x = input:read("*n")
			if x == nil then
				break
			end
			local y = input:read("*n")
			local z = input:read("*n")
			local name = input:read("*l")
			spawns[name:sub(2)] = {x = x, y = y, z = z}
		until input:read(0) == nil
		io.close(input)
	elseif input and bkwd then
		zr_bed.spawn = minetest.deserialize(input:read("*all"))
		input:close()
		zr_bed.save_spawns()
		os.rename(file, file .. ".backup")
		file = org_file
	end
end

zr_bed.read_spawns()

function zr_bed.save_spawns()
	if not zr_bed.spawn then
		return
	end
	local data = {}
	local output = io.open(org_file, "w")
	for k, v in pairs(zr_bed.spawn) do
		table.insert(data, string.format("%.1f %.1f %.1f %s\n", v.x, v.y, v.z, k))
	end
	output:write(table.concat(data))
	io.close(output)
end

function zr_bed.set_spawns()
	for name,_ in pairs(zr_bed.player) do
		local player = minetest.get_player_by_name(name)
		local p = player:get_pos()
		-- but don't change spawn location if borrowing a bed
		if not minetest.is_protected(p, name) then
			zr_bed.spawn[name] = p
		end
	end
	zr_bed.save_spawns()
end

function zr_bed.remove_spawns_at(pos)
	for name, p in pairs(zr_bed.spawn) do
		if vector.equals(vector.round(p), pos) then
			zr_bed.spawn[name] = nil
		end
	end
	zr_bed.save_spawns()
end
