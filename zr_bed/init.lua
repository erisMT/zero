-- beds/init.lua

-- Load support for MT game translation.
local S = minetest.get_translator("zr_bed")
local esc = minetest.formspec_escape

zr_bed = {}
zr_bed.player = {}
zr_bed.bed_position = {}
zr_bed.pos = {}
zr_bed.spawn = {}
zr_bed.get_translator = S

zr_bed.formspec = "size[8,11;true]" ..
	"no_prepend[]" ..
	"bgcolor[#080808BB;true]" ..
	"button_exit[2,10;4,0.75;leave;" .. esc(S("Leave Bed")) .. "]"

local modpath = minetest.get_modpath("zr_bed")

-- Load files

dofile(modpath .. "/functions.lua")
dofile(modpath .. "/api.lua")
dofile(modpath .. "/bed.lua")
dofile(modpath .. "/spawns.lua")
