-- Gold
local S = minetest.get_translator("zr_gold");

local gold_metal = zr_metal.register_metal("zr_gold", {
	block = {description = S("Gold Block"), png = "zr_gold_block.png"},
	ingot = {description = S("Gold Ingot"), png = "zr_gold_ingot.png"},
	lump =  {description = S("Gold Lump"), png = "zr_gold_lump.png"},
	ore =   {description = S("Gold Ore"), png = "zr_gold_mineral.png"},
})
minetest.register_alias("gold:block", "zr_gold:block")
minetest.register_alias("gold:ingot", "zr_gold:ingot")
minetest.register_alias("gold:lump", "zr_gold:lump")
minetest.register_alias("gold:ore", "zr_gold:ore")

if (gold_metal.ore ~= nil) then
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = gold_metal.ore,
		wherein        = "mapgen_stone",
		clust_scarcity = 13 * 13 * 13,
		clust_num_ores = 5,
		clust_size     = 3,
		y_max          = 31000,
		y_min          = 1025,
	})
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = gold_metal.ore,
		wherein        = "mapgen_stone",
		clust_scarcity = 15 * 15 * 15,
		clust_num_ores = 3,
		clust_size     = 2,
		y_max          = -256,
		y_min          = -511,
	})
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = gold_metal.ore,
		wherein        = "mapgen_stone",
		clust_scarcity = 13 * 13 * 13,
		clust_num_ores = 5,
		clust_size     = 3,
		y_max          = -512,
		y_min          = -31000,
	})
end
