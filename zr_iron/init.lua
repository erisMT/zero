
zr_iron = {}

local mod_path = minetest.get_modpath("zr_iron")
dofile(mod_path.."/iron.lua")
dofile(mod_path.."/ladder.lua")

if (minetest.get_modpath("zr_sign") ~= nil) then
	dofile(mod_path.."/sign.lua")
end

if (minetest.get_modpath("zr_door") ~= nil) then
	dofile(mod_path.."/door.lua")
end

if (minetest.get_modpath("simple_armor") ~= nil) then
	dofile(mod_path.."/armor.lua")
end
