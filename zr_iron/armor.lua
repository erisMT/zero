
local S = minetest.get_translator("zr_iron");

-- Boots
simple_armor.register("zr_iron:boots", {
	description = S("Steel Boots"),
	inventory_image = "zr_iron_boots_item.png",
	armor = {
		slot = "feet",
		groups = { fleshy = 8 }, 
		texture = "zr_iron_boots.png",
	},
})
simple_armor.boots_recipe("zr_iron:boots", "zr_iron:iron")
minetest.register_alias("iron:boots", "zr_iron:boots")

-- Chestplate
simple_armor.register("zr_iron:chestplate", {
	description = S("Steel Boots"),
	inventory_image = "zr_iron_chestplate_item.png",
	armor = {
		slot = "torso",
		groups = { fleshy = 16 }, 
		texture = "zr_iron_chestplate.png",
	},
})
simple_armor.chestplate_recipe("zr_iron:chestplate", "zr_iron:iron")
minetest.register_alias("iron:chestplate", "zr_iron:chestplate")

-- Leggings
simple_armor.register("zr_iron:leggings", {
	description = S("Steel Boots"),
	inventory_image = "zr_iron_leggings_item.png",
	armor = {
		slot = "legs",
		groups = { fleshy = 14 }, 
		texture = "zr_iron_leggings.png",
	},
})
simple_armor.leggings_recipe("zr_iron:leggings", "zr_iron:iron")
minetest.register_alias("iron:leggings", "zr_iron:leggings")

-- Helmet
simple_armor.register("zr_iron:helmet", {
	description = S("Steel Boots"),
	inventory_image = "zr_iron_helmet_item.png",
	armor = {
		slot = "head",
		groups = { fleshy = 10 }, 
		texture = "zr_iron_helmet.png",
	},
})
simple_armor.helmet_recipe("zr_iron:helmet", "zr_iron:iron")
minetest.register_alias("iron:helmet", "zr_iron:helmet")

