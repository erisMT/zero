
local S = minetest.get_translator("zr_iron");

minetest.register_node("zr_iron:ladder", {
	description = S("Steel Ladder"),
	drawtype = "signlike",
	tiles = {"zr_iron_ladder.png"},
	inventory_image = "zr_iron_ladder.png",
	wield_image = "zr_iron_ladder.png",
	paramtype = "light",
	paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	climbable = true,
	is_ground_content = false,
	selection_box = { type = "wallmounted" },
	groups = {cracky = 2},
	sounds = zr_metal.sounds,
})
minetest.register_alias("iron:ladder", "zr_iron:ladder")

minetest.register_craft({
	output = "zr_iron:ladder 15",
	recipe = {
		{"zr_iron:ingot", "", "zr_iron:ingot"},
		{"zr_iron:ingot", "zr_iron:ingot", "zr_iron:ingot"},
		{"zr_iron:ingot", "", "zr_iron:ingot"},
	}
})
