
local S = minetest.get_translator("zr_iron");

-- DOOR
zr_door.register("zr_iron:door", {
		tiles = {{name = "zr_iron_door.png", backface_culling = true}},
		description = S("Steel Door"),
		inventory_image = "zr_iron_door_item.png",
		protected = true,
		groups = {node = 1, cracky = 1, level = 2},
		sounds = zr_metal.sounds,
})
minetest.register_alias("iron:door", "zr_iron:door")

-- TRAPDOOR
zr_door.register_trapdoor("zr_iron:trapdoor", {
	description = S("Steel Trapdoor"),
	inventory_image = "zr_iron_trapdoor.png",
	wield_image = "zr_iron_trapdoor.png",
	tile_front = "zr_iron_trapdoor.png",
	tile_side = "zr_iron_trapdoor_side.png",
	protected = true,
	groups = {cracky = 1, level = 2, door = 1},
	sounds = zr_metal.sounds,
})
minetest.register_alias("iron:trapdoor", "zr_iron:trapdoor")
