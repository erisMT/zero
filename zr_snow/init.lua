
zr_snow = {};

local mod_path = minetest.get_modpath("zr_snow")

dofile(mod_path.."/snow.lua")
dofile(mod_path.."/ice.lua")
dofile(mod_path.."/permafrost.lua")
