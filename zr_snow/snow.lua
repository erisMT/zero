
local S = minetest.get_translator("zr_snow")

zr_snow.sounds = {
	footstep = {name = "zr_snow_footstep", gain = 0.2},
	dig = {name = "zr_snow_footstep", gain = 0.3},
	dug = {name = "zr_snow_footstep", gain = 0.3},
	place = {name = "zr_snow_place", gain = 1},
}

minetest.register_node("zr_snow:snow", {
	description = S("Snow"),
	tiles = {"zr_snow.png"},
	inventory_image = "zr_snow_ball.png",
	wield_image = "zr_snow_ball.png",
	paramtype = "light",
	buildable_to = true,
	floodable = true,
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, 0.5, -0.25, 0.5},
		},
	},
	collision_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, 0.5, -6 / 16, 0.5},
		},
	},
	groups = {crumbly = 3, oddly_breakable_by_hand=3, falling_node = 1, snowy = 1},
	sounds = zr_snow.sounds,

	on_construct = function(pos)
		pos.y = pos.y - 1
		if minetest.get_node(pos).name == "zr_dirt:grass" then
			minetest.set_node(pos, {name = "zr_dirt:snow"})
		end
	end,
})
minetest.register_alias("snow:snow","zr_snow:snow")
minetest.register_alias("snow","zr_snow:snow")

minetest.register_node("zr_snow:block", {
	description = S("Snow Block"),
	tiles = {"zr_snow.png"},
	groups = {crumbly = 3, oddly_breakable_by_hand=3, cools_lava = 1, snowy = 1},
	sounds = zr_snow.sounds,

	on_construct = function(pos)
		pos.y = pos.y - 1
		if minetest.get_node(pos).name == "zr_dirt:grass" then
			minetest.set_node(pos, {name = "zr_dirt:snow"})
		end
	end,
})
minetest.register_alias("snow:block","zr_snow:block")

function zr_snow.add_to_biome(biome, def)
	local snow_def =  {
		name = biome..":patchy_snow",
		deco_type = "simple",
		place_on = {
			"zr_snow:permafrost_with_moss",
			"zr_snow:permafrost_with_stones",
			"group:stone",
			"group:dirt",
			"zr_gravel:gravel"
		},
		sidelen = 4,
		noise_params = {
			offset = 0,
			scale = 1.0,
			spread = {x = 100, y = 100, z = 100},
			seed = 172555,
			octaves = 3,
			persist = 1.0
		},
		biomes = { biome },
		y_max = 50,
		y_min = 1,
		decoration = "zr_snow:snow",
	}

	def = def or {}
	for k, v in pairs(def) do 
		snow_def[k] = v 
	end

	minetest.register_decoration(snow_def)

end

