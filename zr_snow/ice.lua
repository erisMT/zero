local S = minetest.get_translator("zr_snow")

zr_snow.ice_sounds = {
	footstep = {name = "zr_snow_ice_footstep", gain = 0.2},
	dig = {name = "zr_snow_ice_dig", gain = 0.3},
	dug = {name = "zr_snow_ice_dug", gain = 0.3},
	place = {name = "zr_snow_ice_footstep", gain = 1},
}

-- 'is ground content = false' to avoid tunnels in sea ice or ice rivers
minetest.register_node("zr_snow:ice", {
	description = S("Ice"),
	tiles = {"zr_snow_ice.png"},
	is_ground_content = false,
	paramtype = "light",
	groups = {cracky = 3, cools_lava = 1, slippery = 3},
	sounds = zr_snow.ice_sounds,
})
minetest.register_alias("snow:ice","zr_snow:ice")

-- Mapgen-placed ice with 'is ground content = true' to contain tunnels
minetest.register_node("zr_snow:cave_ice", {
	description = S("Cave Ice"),
	tiles = {"zr_snow_ice.png"},
	paramtype = "light",
	groups = {cracky = 3, cools_lava = 1, slippery = 3,
		not_in_creative_inventory = 1},
	drop = "zr_snow:ice",
	sounds = zr_snow.ice_sounds,
})
minetest.register_alias("snow:cave_ice","zr_snow:cave_ice")

